﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrefour
{
    class Feu
    {
        private bool estVert;
        List<IUsager> listUsager;

        public Feu(bool pEstVert)
        {
            EstVert = pEstVert;
        }

        public bool EstVert { get => estVert; set => estVert = value; }
    }
}
