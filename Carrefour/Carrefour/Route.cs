﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrefour
{
    class Route
    {
        // Variables
        public string Sens;
        public int PixelDeReference;
        public bool FeuVert;

        /// <summary>
        /// Constructeur de la route
        /// </summary>
        /// <param name="pSens">Le sen de la route</param>
        public Route(string pSens)
        {
            Sens = pSens;
            FeuVert = false;
            switch(pSens)
            {
                case "haut":
                    PixelDeReference = 370;
                    break;
                case "bas":
                    PixelDeReference = 280;
                    break;
                case "gauche":
                    PixelDeReference = 370;
                    break;
                case "droite":
                    PixelDeReference = 280;
                    break;
            }
        }

        /// <summary>
        /// Allume le feu
        /// </summary>
        public void Allumer()
        {
            FeuVert = true;
        }

        /// <summary>
        /// Eteint le feu
        /// </summary>
        public void Eteindre()
        {
            FeuVert = false;
        }
    }
}
